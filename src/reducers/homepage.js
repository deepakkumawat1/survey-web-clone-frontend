import * as actions from '../actions/types'
const initialState = {
  error: '',
  allUserSurveys:[]
}
export default function (state = initialState, action) {
  switch (action.type) {
    
    case actions.ERROR_HOMEPAGE:
      return {
        ...state,
        error: action.payload
      }
      case actions.FETCH_HOMEPAGE:
      return {
        ...state,
        allUserSurveys: action.payload
      }

    default:
      return state
  }
}
