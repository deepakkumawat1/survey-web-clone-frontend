import {combineReducers} from 'redux';
import userReducer from './User'

import dashboardReducer from './dashboard'

import homepageReducer from './homepage'

export default combineReducers({
    user:userReducer,
    userSurveys:dashboardReducer,
    allUserSurveys:homepageReducer

})