import * as actions from '../actions/types'
const initialState = {
  error: '',
  userData:{}
}
export default function (state = initialState, action) {
  switch (action.type) {
    
    case actions.ERROR_LOGIN:
      return {
        ...state,
        error: action.payload
      }
      case actions.FETCH_USER:
      return {
        ...state,
        userData: action.payload
      }

    default:
      return state
  }
}
