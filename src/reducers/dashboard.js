import * as actions from '../actions/types'
const initialState = {
  error: '',
  userSurveys:[]
}
export default function (state = initialState, action) {
  switch (action.type) {
    
    case actions.ERROR_DASHBOARD:
      return {
        ...state,
        error: action.payload
      }
      case actions.FETCH_DASHBOARD:
      return {
        ...state,
        userSurveys: action.payload
      }

    default:
      return state
  }
}
