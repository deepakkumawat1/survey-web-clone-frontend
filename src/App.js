import React from 'react'
import Login from './components/Login'
import SignUp from './components/SignUp'

import NavRoute from './components/NavRoute'
import PrivateRoute from './components/PrivateRoute'
import HomePage from './components/HomePage'
import FillSurvey from './components/FillSurvey'
import Dashboard from './components/Dashboard'

import Result from './components/Result'

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import './App.css'
class App extends React.Component {
  render () {
    return (
      <Router>
        {/* <div>
          <Homepage></Homepage>
        </div> */}
        <Switch>
        <NavRoute exact component={Dashboard} path='/dashboard' />

          <NavRoute exact component={HomePage} path='/' />
          <NavRoute exact component={FillSurvey} path='/fillSurvey/:surevyId/:type' />
          <NavRoute exact component={PrivateRoute} path='/createSurveys' />
          <NavRoute exact component={Result} path='/surveyResponse/result/:surveyId/:type' />

          {/* <Route path='/' exact >
            <NavRoute />
          </Route> */}
          <Route path='/login'>
            <Login />
          </Route>
          <Route path='/signup'>
            <SignUp />
          </Route>
        </Switch>
      </Router>
    )
  }
}

export default App
