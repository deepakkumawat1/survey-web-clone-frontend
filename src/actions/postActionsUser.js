import * as actions from './types'

export const validationErrorExplicitNull= ()=> (dispatch) => {
    dispatch({
        type:actions.ERROR_LOGIN,
        payload: null
    })
}
export const fetchUser = (email,password) => (dispatch) => {
    const requestOptions = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email: email,
          password: password
        })
      }
      fetch('https://survey-monkey-clone-backend1.herokuapp.com/api/user/login', requestOptions)
        .then(response => response.json())
        .then(data => {
          console.log(data)
  
        //   setResponse(data)
          if(data.error){
            dispatch({
                type:actions.ERROR_LOGIN,
                payload: data.error
            })
            //   setError(data.error)
          }
          if(data.token) {
            dispatch({
                type:actions.FETCH_USER,
                payload: data.user
            })
            localStorage.setItem('auth-token',data.token);
            // history.push("/dashboard");
              // return <Redirect to="/" />
           }
        })
        .catch(e => console.log(e))
    
}