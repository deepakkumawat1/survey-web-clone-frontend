import * as actions from './types'

export const validationErrorExplicitNullDashbd = () => (dispatch) =>{
    dispatch({
        type:actions.ERROR_DASHBOARD,
        payload: null
    })
}
export const fetchDashboard = (userId) => (dispatch) => {
    let requestoption={
        method: 'get', 
   headers: new Headers({
     // Your header content
     'auth-token':localStorage.getItem("auth-token")
   })
       
    }
    fetch(`https://survey-monkey-clone-backend1.herokuapp.com/api/surveys/${userId}`,requestoption)
      .then(response => response.json())
      .then(data => {
        // console.log(data)

        if(data.error){
            console.log('action dashboard..................error')

            console.log(data.error)
            dispatch({
                type:actions.ERROR_DASHBOARD,
                payload: data.error
            })
           
          }
          else{
            dispatch({
                type:actions.FETCH_DASHBOARD,
                payload: data
            })
          }
        console.log(data) 
        
      })
      .catch(e => console.log(e))
    
}