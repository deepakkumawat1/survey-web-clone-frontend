import * as actions from './types'


export const fetchHomepage = () => (dispatch) => {
    fetch(`https://survey-monkey-clone-backend1.herokuapp.com/`)
      .then(response => response.json())
      .then(data => {
        if(data.error){
            dispatch({
                type:actions.ERROR_HOMEPAGE,
                payload: data.error
            })
            //   setError(data.error)
          }
          if(data.message){
            dispatch({
                type:actions.FETCH_HOMEPAGE,
                payload: data
            })
          }
        console.log(data) 
        
      })
      .catch(e => console.log(e))
    
}