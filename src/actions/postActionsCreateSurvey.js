import * as actions from './types'

export const fetchUser = (email, password) => dispatch => {
  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      email: email,
      password: password
    })
  }
  fetch(
    'https://survey-monkey-clone-backend1.herokuapp.com/api/user/login',
    requestOptions
  )
    .then(response => response.json())
    .then(data => {
      console.log(data)

      if (data.error) {
        dispatch({
          type: actions.ERROR_CREATE_SURVEY,
          payload: data.error
        })
      }
      if (data.message) {
        dispatch({
          type: actions.CREATE_SURVEY,
          payload: data
        })
        localStorage.setItem('auth-token', data.token)
      }
    })
    .catch(e => console.log(e))
}
