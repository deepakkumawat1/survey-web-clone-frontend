import React, { useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { fetchHomepage } from '../actions/postActionHomepage'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import {
  CssBaseline,
  Typography,
  Container,
  Paper,
  Box,
  Grid
} from '@material-ui/core'

import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(5),
    textAlign: 'center',
    color: theme.palette.text.secondary
  },
  closeButton: {
    background: 'green',
    color: 'white',
    '&:hover': {
      //you want this to be the same as the backgroundColor above
      backgroundColor: 'green'
    }
  }
}))
function Dashboard ({ userSurveys, fetchHomepage }) {
  const history = useHistory()
  const routeChange = () => {
    history.push('/fillSurvey')
  }
//   const handleResultRoute = () => {
//     history.push('/surveyResponse/result')
//   }
  useEffect(() => {
    // fetchH(userData._id)
    fetchHomepage()
    console.log(userSurveys)
  }, [])
  

  const classes = useStyles()
  return (
    <React.Fragment>
      <CssBaseline />
      <Container>
        <Box my={3}>
          <Typography variant='h3' gutterBottom>
            Open Surveys
          </Typography>
        </Box>
        <div className={classes.root}>
          <Grid container spacing={1}>
            {userSurveys.message &&
              userSurveys.message.map(survey => {
                if (survey.isOpen === true)
                  return (
                    <Grid key={survey._id} item lg={2}>
                      {' '}
                      <Link to={`/fillSurvey/${survey._id}/homepage`}>
                        <Paper
                          elevation={12}
                          className={classes.paper}
                          // onClick={routeChange}
                        >
                          {survey.surveyName}
                          
                        </Paper>{' '}
                      </Link>
                    </Grid>
                  )
              })}
          </Grid>
        </div>

        <Box my={3}>
          <Typography variant='h3' gutterBottom>
            Closed Surveys
          </Typography>
        </Box>
        <div className={classes.root}>
          <Grid container spacing={1}>
            {userSurveys.message &&
              userSurveys.message.map(survey => {
                if (survey.isOpen === false)
                  return (
                    <Grid key={survey._id} item lg={2}>
                      {' '}
                      <Link
                        to={`/surveyResponse/result/${survey._id}/homepage`}
                      >
                        <Paper
                          elevation={12}
                          className={classes.paper}
                          onClick={routeChange}
                        >
                          {survey.surveyName}
                        </Paper>{' '}
                      </Link>
                    </Grid>
                  )
              })}
          </Grid>
        </div>
      </Container>
    </React.Fragment>
  )
}
const mapStateToProps = state => ({
  userData: state.user.userData,
  error: state.allUserSurveys.error,
  userSurveys: state.allUserSurveys.allUserSurveys
})

export default connect(mapStateToProps, { fetchHomepage })(Dashboard)
