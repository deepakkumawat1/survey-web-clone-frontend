import React from 'react'
import Navbar from './Navbar'

import { Route } from 'react-router-dom'
const NavRoute = ({ exact, path, component: Component, ...rest }) => {
  return (
    <Route
      exact={exact}
      path={path}
      render={props => (
        <div>
          <Navbar />
          <Component {...props} />
        </div>
      )}
    />
  )
}

export default NavRoute
