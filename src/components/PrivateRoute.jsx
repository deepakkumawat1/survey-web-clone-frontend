import React from 'react'
import { useHistory } from 'react-router-dom'
import { connect } from 'react-redux'
import Errorhandler from './Error'

import CssBaseline from '@material-ui/core/CssBaseline'
import Container from '@material-ui/core/Container'
import TextField from '@material-ui/core/TextField'
import Paper from '@material-ui/core/Paper'
import { makeStyles } from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Collapse from '@material-ui/core/Collapse'
import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'
import SaveIcon from '@material-ui/icons/Save'
import CloseIcon from '@material-ui/icons/Close'
import {
  Button,
  Grid,
  FormControl,
  Box,
  InputAdornment
} from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'
const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  },
  nested: {
    paddingLeft: theme.spacing(4)
  },
  button: {
    margin: theme.spacing(1)
  },
  paperMargin: {
    padding: theme.spacing(2),
    paddingTop: theme.spacing(4)
  },
  alert: {
    width: '100%'
  }
}))
 function PrivateRoute ({userData}) {
  const classes = useStyles()
  const [addQuestion, setAddQuestion] = React.useState(true)

  const [question, setQuestion] = React.useState('')
  const [optionA, setOptionA] = React.useState('')
  const [optionB, setOptionB] = React.useState('')
  const [optionC, setOptionC] = React.useState('')
  const [optionD, setOptionD] = React.useState('')
  const [questionList, setQuestionList] = React.useState([])
  const [error, setError] = React.useState('')
  const [title, setTitle] = React.useState('')
  const [open, setOpen] = React.useState([])
  const [count, setCount] = React.useState(1)
  

  const history = useHistory()

  const handleClick = (e,i) => {
      console.log("handle clicked")

      console.log(open)
      open[i]=!(open[i])
      let a=open
    setOpen(a)
    setCount(count+1)
  }
  const handleAddQuestion = () => {
    setAddQuestion(!addQuestion)
  }
  const handleSave = () => {
    let body = {
      surveyName: title,
      isOpen: true,
      questions: questionList
    }
    console.log(body)
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    }
    fetch(`https://survey-monkey-clone-backend1.herokuapp.com/createSurveys/${userData._id}`, requestOptions)
      .then(response => response.json())
      .then(data => { 
        console.log(data)

        //   setResponse(data)
        if (data.error) {
            console.log(data)
          setError(data.error)
        }
        if (data.message) {
          history.push('/')
          console.log(data)
        }
      })
      .catch(e => console.log(e))
  }
  const handleAddQuestionList = (
    question,
    optionA,
    optionB,
    optionC,
    optionD
  ) => {
   
    let a = optionA
    let b = optionB
    let c = optionC
    let d = optionD

    setOpen([...open,false])
    setQuestionList([...questionList, {question, a, b, c, d}])
    if (questionList.length === 9) {
      setAddQuestion(!addQuestion)
    }
    console.log([...open,false])
    console.log(userData,[...questionList, {question, a, b, c, d}])
  }
  return (
    localStorage.getItem("auth-token") ?
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth='sm'>
        <Box pt={6}>
          <Paper elevation={17} className={classes.paperMargin}>
            {' '}
            <Container>
              <TextField id='outlined-basic' label='Title' variant='outlined' value={title}
            onChange={e => {
              setTitle(e.target.value)
            }}/>
              {error && (
                <Alert className={classes.alert} severity='error'>
                  {' '}
                  {error}
                </Alert>
              )}

              {questionList.map((question,i) => (
                <List key={i}>
                  <ListItem button onClick={(e)=>handleClick(e,i)}>
                    
                    <ListItemText primary={question.question} />
                    {console.log(open)}
                    {open[i] ? <ExpandLess /> : <ExpandMore />}
                  </ListItem>
                  <Collapse in={open[i]} timeout='auto' unmountOnExit>
                    <List component='div' disablePadding>
                      <ListItem button className={classes.nested}>
                        
                        <ListItemText primary={question.a} />
                      </ListItem>
                    </List>
                    <List component='div' disablePadding>
                      <ListItem button className={classes.nested}>
                        
                        <ListItemText primary={question.b} />
                      </ListItem>
                    </List>
                    <List component='div' disablePadding>
                      <ListItem button className={classes.nested}>
                        
                        <ListItemText primary={question.c} />
                      </ListItem>
                    </List>
                    <List component='div' disablePadding>
                      <ListItem button className={classes.nested}>
                        
                        <ListItemText primary={question.d} />
                      </ListItem>
                    </List>
                  </Collapse>
                </List>
              ))}

              <Box m={1}>
                <Grid container direction='row' justify='flex-end'>
                  {questionList.length === 10 ? (
                    <Alert className={classes.alert} severity='error'>
                      {' '}
                      Max. Questions 10
                    </Alert>
                  ) : (
                    <Box></Box>
                  )}
                  {addQuestion ? (
                    questionList.length === 10 ? (
                      <Box></Box>
                    ) : (
                      <Button
                        variant='contained'
                        className={classes.button}
                        onClick={handleAddQuestion}
                      >
                        Add Question
                      </Button>
                    )
                  ) : (
                    <Button
                      variant='contained'
                      className={classes.button}
                      onClick={handleAddQuestion}
                    >
                      X
                    </Button>
                  )}
                </Grid>
                {addQuestion ? (
                  <Box></Box>
                ) : (
                  <Box>
                    <Grid item md={10}>
                      <Paper elevation={0} className={classes.paper}>
                        {' '}
                        <FormControl fullWidth>
                          {' '}
                          <TextField
                            id='outlined-basic'
                            label='Question'
                            value={question}
                            onChange={e => {
                              setQuestion(e.target.value)
                            }}
                          />
                        </FormControl>
                      </Paper>
                    </Grid>
                    {/* {data.map(data => ( */}
                    <Grid item md={3}>
                      <Paper elevation={0} className={classes.paper}>
                        {' '}
                        <FormControl fullWidth>
                          {' '}
                          <TextField
                            value={optionA}
                            onChange={e => {
                              setOptionA(e.target.value)
                            }}
                            id='outlined-basic'
                            label='Option'
                            InputProps={{
                              startAdornment: (
                                <InputAdornment position='start'>
                                  {/* {data} */}a
                                </InputAdornment>
                              )
                            }}
                          />
                        </FormControl>
                      </Paper>
                    </Grid>
                    <Grid item md={3}>
                      <Paper elevation={0} className={classes.paper}>
                        {' '}
                        <FormControl fullWidth>
                          {' '}
                          <TextField
                            value={optionB}
                            onChange={e => {
                              setOptionB(e.target.value)
                            }}
                            id='outlined-basic'
                            label='Option'
                            InputProps={{
                              startAdornment: (
                                <InputAdornment position='start'>
                                  b
                                </InputAdornment>
                              )
                            }}
                          />
                        </FormControl>
                      </Paper>
                    </Grid>
                    <Grid item md={3}>
                      <Paper elevation={0} className={classes.paper}>
                        {' '}
                        <FormControl fullWidth>
                          {' '}
                          <TextField
                            value={optionC}
                            onChange={e => {
                              setOptionC(e.target.value)
                            }}
                            id='outlined-basic'
                            label='Option'
                            InputProps={{
                              startAdornment: (
                                <InputAdornment position='start'>
                                  c
                                </InputAdornment>
                              )
                            }}
                          />
                        </FormControl>
                      </Paper>
                    </Grid>
                    <Grid item md={3}>
                      <Paper elevation={0} className={classes.paper}>
                        {' '}
                        <FormControl fullWidth>
                          {' '}
                          <TextField
                            value={optionD}
                            onChange={e => {
                              setOptionD(e.target.value)
                            }}
                            id='outlined-basic'
                            label='Option'
                            InputProps={{
                              startAdornment: (
                                <InputAdornment position='start'>
                                  d
                                </InputAdornment>
                              )
                            }}
                          />
                        </FormControl>
                      </Paper>
                    </Grid>
                    {/* ))} */}
                  </Box>
                )}
              </Box>
              <Grid container direction='row' justify='flex-end'>
                {!addQuestion && (
                  <Button
                    variant='contained'
                    color='primary'
                    onClick={() =>
                      handleAddQuestionList(
                        question,
                        optionA,
                        optionB,
                        optionC,
                        optionD
                      )
                    }
                  >
                    Add to question list
                  </Button>
                )}
              </Grid>
              <Grid container direction='row' justify='flex-end'>
                <Button
                  variant='contained'
                  color='primary'
                  size='small'
                  className={classes.button}
                  startIcon={<SaveIcon />}
                  onClick={handleSave}
                >
                  Save
                </Button>
                <Button
                  variant='contained'
                  color='primary'
                  // size="small"
                  className={classes.button}
                  startIcon={<CloseIcon />}
                  onClick={()=> {history.push('/')}}
                >
                  Close
                </Button>
              </Grid>
            </Container>
          </Paper>
        </Box>
      </Container>
    </React.Fragment>
  :<div><Errorhandler message={'Access denied'} detail={' Please LogIn First! '}/></div>
  )
}
const mapStateToProps = state => ({
    userData: state.user.userData,
  }) 

export default connect(mapStateToProps)(PrivateRoute)