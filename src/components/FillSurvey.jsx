import React, { Component } from 'react'
import { connect } from 'react-redux'
// import {useHistoimport Pusher from 'pusher-js'ry} from 'react-router-dom'

// import * as Pusher from 'pusher';

// var Pusher = require('pusher');

import {
  CssBaseline,
  Container,
  Typography,
  Box,
  Paper,
  TextField,
  Grid,
  Button
} from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormControl from '@material-ui/core/FormControl'
import FormLabel from '@material-ui/core/FormLabel'
import Alert from '@material-ui/lab/Alert'

const styles = theme => ({
  root: {
    backgroundColor: 'red'
  },
  bottom1: {
    flexGrow: 1
  },
  title: {
    textAlign: 'center',
    fontSize: '2em'
  },

  bottom: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary
  },
  cancelBtn: {
    marginRight: theme.spacing(2)
  }
})
class FillSurvey extends Component {
  constructor (props) {
    super(props)
    this.state = {
      value: [],
      surveyIdh: '5eed4b3dabf55d3a55693f5e',
      currentSurveyh: [
        {
          _id: '5eed4b3dabf55d3a55693f5e',
          user_id: '5eec748dcd52e512a61ff4d7',
          surveyName: 'my hobby',
          isOpen: true,
          questions: [
            {
              _id: '5eed4b3dabf55d3a55693f5f',
              question: 'what is your name',
              a: 'deepak',
              b: 'ram ',
              c: 'shyam',
              d: 'gita'
            },
            {
              _id: '5eed4b3dabf55d3a55693f60',
              question: 'what is your mothers name',
              a: 'mahi ',
              b: 'rima',
              c: 'nanan',
              d: 'kkfkf'
            },
            {
              _id: '5eed4b3dabf55d3a55693f61',
              question: 'what is your fathers name',
              a: 'sonu',
              b: 'monu',
              c: 'kishan',
              d: 'bhavnana'
            }
          ]
        }
      ]
    }
  }
  componentDidMount () {
    const surveyId = this.props.match.params.surevyId
    console.log(this.props.match.params)
    let currentSurvey
    if (this.props.match.params.type === 'homepage') {
      currentSurvey = this.props.allUserSurveys.message.filter(
        survey => survey._id === this.props.match.params.surevyId
      )
      console.log('homepage')
    } else {
      currentSurvey = this.props.userSurveys.filter(
        survey => survey._id === surveyId
      )
      console.log('dashboard')
    }
    // const currentSurvey=this.props.allUserSurveys.message.filter( (survey) => survey._id === this.props.match.params.surevyId)

    // const currentSurvey=this.props.userSurveys.filter( (survey) => survey._id === surveyId)
    // console.log(currentSurvey)
    // currentSurvey[0].questions
    this.setState({ currentSurvey, surveyId })
    let value = []
    for (let i = 0; i <= currentSurvey[0].questions.length; i++) {
      value.push('')
    }
    this.setState({ value })
  }
  handleCancel = () => {
    this.props.history.push('/')
  }
  handleSubmit = () => {
    let answers = []
    let currentSurvey = this.state.currentSurvey[0].questions
    let value = this.state.value
    for (let i = 0; i < currentSurvey.length; i++) {
      for (const property in currentSurvey[i]) {
        if (value[i] === currentSurvey[i][property]) {
          console.log({ question: currentSurvey[i].question, answer: property })
          answers.push({
            question: currentSurvey[i].question,
            answer: property
          })
        }
      }
    }
    let responseStructure = { email: this.state.email, answers }
    console.log(responseStructure)
    const requestOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(responseStructure)
    }
    fetch(
      `https://survey-monkey-clone-backend1.herokuapp.com/surveyResponse/${this.state.surveyId}`,
      requestOptions
    )
      .then(response => response.json())
      .then(data => {
        console.log(data)

        if (data.error) {
          this.setState({ error: data.error })
        }
        if (data.message) {
          console.log('successfully saved response')
           setTimeout(() => {
            this.props.history.push(
                `/surveyResponse/result/${this.state.surveyId}/homepage`
              )          }, 1000);
          
        }
      })
      .catch(e => console.log(e))
  }
  handleChange = (e, i) => {
    console.log(e.target)
    let value = this.state.value
    value[i] = e.target.value
    this.setState({ value })
  }
  render () {
    const { classes, match } = this.props
    let currentSurvey
    console.log(match)

    console.log(currentSurvey)
    if (this.props.match.params.type === 'homepage') {
      currentSurvey = this.props.allUserSurveys.message.filter(
        survey => survey._id === this.props.match.params.surevyId
      )
      console.log('homepage')
    } else {
      currentSurvey = this.props.userSurveys.filter(
        survey => survey._id === this.props.match.params.surevyId
      )
      console.log('dashboard')
    }

    return (
      <React.Fragment>
        <CssBaseline />
        <Container maxWidth='sm'>
          <Paper elevation={1}>
            <Box p={2} mx={'auto'}>
              <Typography
                component='div'
                className={classes.title}
              ></Typography>
              {this.state.error && (
                <Alert className={classes.alert} severity='error'>
                  {' '}
                  {this.state.error}
                </Alert>
              )}
              {currentSurvey[0].questions.map((question, i) => (
                <Box>
                  <FormControl key={i} component='fieldset'>
                    <FormLabel component='legend'>
                      Q{`${i + 1}`} {question.question}
                    </FormLabel>
                    <RadioGroup
                      aria-label={`${question.question}`}
                      name={`${question.question}`}
                      value={this.state.value[i]}
                      onChange={e => this.handleChange(e, i)}
                    >
                      <FormControlLabel
                        value={question.a}
                        control={<Radio />}
                        label={`${question.a}`}
                      />
                      <FormControlLabel
                        value={question.b}
                        control={<Radio />}
                        label={`${question.b}`}
                      />
                      <FormControlLabel
                        value={question.c}
                        control={<Radio />}
                        label={`${question.c}`}
                      />
                      <FormControlLabel
                        value={question.d}
                        control={<Radio />}
                        label={`${question.d}`}
                      />
                    </RadioGroup>
                  </FormControl>
                </Box>
              ))}

              <form noValidate autoComplete='off'>
                <TextField
                  id='outlined-basic'
                  label='Email'
                  placeholder='abc@Yahoo.com'
                  variant='outlined'
                  style={{ width: '70%' }}
                  value={this.state.email}
                  onChange={e => {
                    this.setState({ email: e.target.value })
                  }}
                />
              </form>
              <div className={classes.bottom1}>
                <Grid container justify='flex-end'>
                  <Grid>
                    <Paper elevation={0} className={classes.bottom}>
                      <Button
                        variant='contained'
                        className={classes.cancelBtn}
                        onClick={this.handleCancel}
                      >
                        Cancel
                      </Button>
                      <Button
                        variant='contained'
                        color='primary'
                        onClick={this.handleSubmit}
                      >
                        Submit
                      </Button>
                    </Paper>
                  </Grid>
                </Grid>
              </div>
            </Box>
          </Paper>
        </Container>
      </React.Fragment>
    )
  }
}
const mapStateToProps = state => ({
  userData: state.user.userData,
  error: state.userSurveys.error,
  userSurveys: state.userSurveys.userSurveys,
  allUserSurveys: state.allUserSurveys.allUserSurveys
})
export default connect(mapStateToProps)(
  withStyles(styles, { withTheme: true })(FillSurvey)
)
