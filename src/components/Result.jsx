import React, { Component } from 'react'
import { connect } from 'react-redux'
import Pusher from 'pusher-js'
import {
  CssBaseline,
  Container,
  Typography,
  Box,
  Paper,
  Grid
} from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
const styles = theme => ({
  root: {
    backgroundColor: 'red'
  },
  bottom1: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(5),
    textAlign: 'center',
    color: theme.palette.text.secondary
  },

  title: {
    fontSize: '1.5em'
  },
  option: {
    fontSize: '1em'
  },
  optionContainer: {
    display: 'flex',
    justifyContent: 'space-between'
  }
})
class Result extends Component {
  constructor (props) {
    super(props)
    this.state = {
      value: ''
    }
  }
  componentDidMount () {
    var pusher = new Pusher('41762cfaee540316c970', {
      cluster: 'ap2'
    })

    var channel = pusher.subscribe('survey')
    channel.bind('survey-response', data => {
      console.log(this.state)

      this.handleDataArrange([...this.state.allResponse, data.response])
    })
    let url = `https://survey-monkey-clone-backend1.herokuapp.com/surveyResponse/result/${this.props.match.params.surveyId}`

    fetch(url)
      .then(response => response.json())
      .then(data => {
        if (data.message) {
          let allResponse = data.response
          console.log(allResponse)
          this.setState(allResponse)
          this.handleDataArrange(allResponse)
        }
      })
      .catch(e => console.log(e))
  }

  handleDataArrange = allResponse => {
    let votesCount = allResponse.reduce((votes, response) => {
      console.log(response)
      let a = response.answers.reduce((acc, question) => {
        if (acc[question.question] === undefined) {
          acc[question.question] = {
            [question.answer]: 1
          }
        } else {
          if (acc[question.question][question.answer] === undefined) {
            acc[question.question][question.answer] = 1
          } else {
            acc[question.question][question.answer] =
              acc[question.question][question.answer] + 1
          }
        }
        return acc
      }, votes)

      return a
    }, {})

    let currentSurvey
    if (this.props.match.params.type === 'homepage') {
      currentSurvey = this.props.allUserSurveys.message.filter(
        survey => survey._id === this.props.match.params.surveyId
      )
    } else {
      currentSurvey = this.props.userSurveys.filter(
        survey => survey._id === this.props.match.params.surveyId
      )
    }

    let questionsArray = currentSurvey[0].questions

    console.log(votesCount)
    let finalRsultsSurevy = questionsArray.map((questionObj, i) => {
      let a = {}
      a['question'] = questionObj.question
      if (votesCount[questionObj.question].a === undefined) {
        a['optionA'] = {
          value: questionObj.a,
          counts: 0
        }
      } else {
        a['optionA'] = {
          value: questionObj.a,
          counts: votesCount[questionObj.question].a
        }
      }
      if (votesCount[questionObj.question].b === undefined) {
        a['optionB'] = {
          value: questionObj.b,
          counts: 0
        }
      } else {
        a['optionB'] = {
          value: questionObj.b,
          counts: votesCount[questionObj.question].b
        }
      }
      if (votesCount[questionObj.question].c === undefined) {
        a['optionC'] = {
          value: questionObj.c,
          counts: 0
        }
      } else {
        a['optionC'] = {
          value: questionObj.c,
          counts: votesCount[questionObj.question].c
        }
      }
      if (votesCount[questionObj.question].d === undefined) {
        a['optionD'] = {
          value: questionObj.d,
          counts: 0
        }
      } else {
        a['optionD'] = {
          value: questionObj.d,
          counts: votesCount[questionObj.question].d
        }
      }
      return a
    })
    console.log(finalRsultsSurevy)

    this.setState({
      allResponse,
      finalRsultsSurevy,
      surveyName: currentSurvey[0].surveyName
    })
  }
  render () {
    let a = this.state.allResponse
    console.log(a)
    console.log(this.state.allResponse)

    const { classes } = this.props
    return (
      <React.Fragment>
        <CssBaseline />
        <Container maxWidth='sm'>
          <Paper elevation={1}>
            <Box p={2} mx={'auto'}>
              <Typography component='div' className={classes.title}>
                Result
              </Typography>

              <Box mb={2}> </Box>
              <Grid container>
                <Grid item lg={4}>
                  {' '}
                  <Paper elevation={5} className={classes.paper}>
                    {this.state.surveyName}
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={2}>
                {' '}
                <hr></hr>
              </Box>
              {this.state.finalRsultsSurevy &&
                this.state.finalRsultsSurevy.map((question, i) => {
                  return (
                    <Box key={i}>
                      <Typography component='div' className={classes.title}>
                        {question.question}
                      </Typography>
                      <Box className={classes.optionContainer} pr={12} pl={2}>
                        <Typography component='div' className={classes.option}>
                          a.{question.optionA.value}
                        </Typography>
                        <Typography component='div' className={classes.option}>
                          {question.optionA.counts} votes
                        </Typography>
                      </Box>
                      <Box className={classes.optionContainer} pr={12} pl={2}>
                        <Typography component='div' className={classes.option}>
                          b.{question.optionB.value}
                        </Typography>
                        <Typography component='div' className={classes.option}>
                          {question.optionB.counts} votes
                        </Typography>
                      </Box>{' '}
                      <Box className={classes.optionContainer} pr={12} pl={2}>
                        <Typography component='div' className={classes.option}>
                          c.{question.optionC.value}
                        </Typography>
                        <Typography component='div' className={classes.option}>
                          {question.optionC.counts} votes
                        </Typography>
                      </Box>{' '}
                      <Box className={classes.optionContainer} pr={12} pl={2}>
                        <Typography component='div' className={classes.option}>
                          d.{question.optionD.value}
                        </Typography>
                        <Typography component='div' className={classes.option}>
                          {question.optionD.counts} votes
                        </Typography>
                      </Box>
                    </Box>
                  )
                })}
            </Box>
          </Paper>
        </Container>
      </React.Fragment>
    )
  }
}
const mapStateToProps = state => ({
  userData: state.user.userData,
  error: state.userSurveys.error,
  userSurveys: state.userSurveys.userSurveys,
  allUserSurveys: state.allUserSurveys.allUserSurveys
})
export default connect(mapStateToProps)(
  withStyles(styles, { withTheme: true })(Result)
)
