import React, { Component } from 'react'
import logo from '../assets/surveymonkey-icon-logo-svg-vector.svg'
import { Link, withRouter } from 'react-router-dom'
class Navbar extends Component {
  handleLogOut = () => {
    document.title = `Survey Monkey Clone`
    localStorage.clear()
    this.setState({})
    this.props.history.push('/')
    console.log(this.props)
  }
  render () {
    return (
      <nav className='navbar navbar-dark bg-dark p-3'>
        <div className='nav-icon'>
          <Link className='text-link' to='/'>
            <img
              src={logo}
              width='30'
              height='30'
              className='d-inline-block'
              alt=''
            ></img>
            <span className='px-2'>Survey Monkey Clone</span>
          </Link>
        </div>

        <ul>
          <li className='px-2'>
            <Link className='text-link' to='/dashboard'>
              My Survey
            </Link>
          </li>
          <li className='px-2'>
            <Link className='text-link' to='/createSurveys'>
              Create Survey
            </Link>
          </li>
          {localStorage.getItem('auth-token') ? (
            <li className='px-2 text-link' onClick={this.handleLogOut}>
              LogOut
            </li>
          ) : (
            <React.Fragment>
              <li className='px-2'>
                <Link className='text-link' to='/login'>
                  Login
                </Link>
              </li>
              <li className='px-2'>
                <Link className='text-link' to='/signup'>
                  SignUp
                </Link>
              </li>
            </React.Fragment>
          )}
        </ul>
      </nav>
    )
  }
}

export default withRouter(Navbar)
