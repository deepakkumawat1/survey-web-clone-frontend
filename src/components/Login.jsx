import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import {
  fetchUser,
  validationErrorExplicitNull
} from '../actions/postActionsUser'
import { validationErrorExplicitNullDashbd } from '../actions/actionDashboard'

import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import TextField from '@material-ui/core/TextField'
import Link from '@material-ui/core/Link'
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import { Link as RouterLink, useHistory } from 'react-router-dom'
import Alert from './Alert'

function Copyright () {
  return (
    <Typography variant='body2' color='textSecondary' align='center'>
      {'Copyright © '}
      <Link color='inherit' href='https://material-ui.com/'>
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  )
}

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}))

function SignIn ({
  error,
  userData,
  fetchUser,
  validationErrorExplicitNull,
  validationErrorExplicitNullDashbd
}) {
  const classes = useStyles()
  const [password, setPassword] = useState('')

  const [email, setEmail] = useState('')
  const history = useHistory()
  const handleSignIn = e => {
    e.preventDefault()
    fetchUser(email, password)
    console.log(userData)
  }
  useEffect(() => {
    localStorage.clear()

    validationErrorExplicitNull()
    validationErrorExplicitNullDashbd()
  }, [])
  useEffect(() => {
    if (localStorage.getItem('auth-token')) {
      console.log(userData)
      history.push('/dashboard')
      document.title = `${userData.name}`
    }

    if (document.title !== 'React App') {
    }
    //
  })
  return (
    <Container component='main' maxWidth='xs'>
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component='h1' variant='h5'>
          Sign in
        </Typography>
        {error && <Alert text={error} />}
        <form className={classes.form} noValidate>
          <TextField
            variant='outlined'
            margin='normal'
            required
            fullWidth
            id='email'
            label='Email Address'
            name='email'
            autoComplete='email'
            autoFocus
            value={email}
            onChange={e => {
              setEmail(e.target.value)
            }}
          />
          <TextField
            variant='outlined'
            margin='normal'
            required
            fullWidth
            name='password'
            label='Password'
            type='password'
            id='password'
            autoComplete='current-password'
            value={password}
            onChange={e => {
              setPassword(e.target.value)
            }}
          />
          {/* <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          /> */}
          <Button
            type='submit'
            fullWidth
            variant='contained'
            color='primary'
            className={classes.submit}
            onClick={handleSignIn}
          >
            Sign In
          </Button>
          <Grid container justify='flex-end'>
            {/* <Grid item xs>
              <Link href="#" variant="body2">
                Forgot password?
              </Link>
            </Grid> */}
            <Grid item>
              <RouterLink to='/signup'>
                {"Don't have an account? Sign Up"}
              </RouterLink>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  )
}
const mapStateToProps = state => ({
  userData: state.user.userData,
  error: state.user.error
})

export default connect(mapStateToProps, {
  fetchUser,
  validationErrorExplicitNull,
  validationErrorExplicitNullDashbd
})(SignIn)
