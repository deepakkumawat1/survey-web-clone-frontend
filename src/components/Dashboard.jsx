import React, { useEffect } from 'react'
import { fetchDashboard,validationErrorExplicitNullDashbd } from '../actions/actionDashboard'
import { Link } from 'react-router-dom'
import Errorhandler from './Error'
import { useHistory } from 'react-router-dom'
import { connect } from 'react-redux'

import {
  CssBaseline,
  Typography,
  Container,
  Paper,
  Button,
  Box,
  Grid
} from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import { makeStyles } from '@material-ui/core/styles'
import CheckIcon from '@material-ui/icons/Check'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(5),
    textAlign: 'center',
    color: theme.palette.text.secondary
  },
  closeButton: {
    background: 'green',
    color: 'white',
    '&:hover': {
      //you want this to be the same as the backgroundColor above
      backgroundColor: 'green'
    }
  }
}))
function Dashboard ({ userData, fetchDashboard, error, userSurveys }) {
  const history = useHistory()

  const routeChange = () => {
    history.push('/fillSurvey')
  }
  const classes = useStyles()
  const handleChangeStatue = id => {
    const requestOptions = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      }
    }
    fetch(`https://survey-monkey-clone-backend1.herokuapp.com/surveys/changeStatus/${id}`, requestOptions)
      .then(response => response.json())
      .then(data => {
        console.log(data)

        
        if (data.message) {
          console.log('successfully saved response')
          fetchDashboard(userData._id)
        
        }
      })
      .catch(e => console.log(e))
    console.log('handlechangestatues', id)
  }
  useEffect(() => {
    fetchDashboard(userData._id)
    
    console.log(userSurveys)
  }, [])

  console.log(userSurveys)
 
  return localStorage.getItem('auth-token')  ? 
    <React.Fragment>
        {error ? <div><Errorhandler message={error} detail={' Please Create New Survey! '}/></div>: 
        <React.Fragment>
          <CssBaseline />
          <Container>
            <Box my={3}>
              <Typography variant='h3' gutterBottom>
                Open Surveys
              </Typography>
            </Box>

            <div className={classes.root}>
              <Grid container spacing={1}>
                {userSurveys.map(survey => {
                  if (survey.isOpen === true)
                    return (
                      <Grid key={survey._id} item lg={2}>
                        {' '}
                        <Link to={`/fillSurvey/${survey._id}/dashboard`}>
                          <Paper
                            elevation={12}
                            className={classes.paper}
                            onClick={routeChange}
                          >
                            {survey.surveyName}
                            {console.log(survey)}
                          </Paper>{' '}
                        </Link>
                        <Button
                          variant='contained'
                          color='secondary'
                          style={{ width: '100%' }}
                          startIcon={<CloseIcon />}
                          className={classes.button}
                          onClick={() => handleChangeStatue(survey._id)}
                        >
                          Close
                        </Button>
                      </Grid>
                    )
                })}
              </Grid>
            </div>
            <Box my={3}>
              <Typography variant='h3' gutterBottom>
                Closed Surveys
              </Typography>
            </Box>

            <div className={classes.root}>
              <Grid container spacing={1}>
                {userSurveys.map(survey => {
                  if (survey.isOpen === false)
                    return (
                      <Grid key={survey._id} item lg={2}>
                        {' '}
                        <Link
                          to={`/surveyResponse/result/${survey._id}/dashboard`}
                        >
                          <Paper
                            elevation={12}
                            className={classes.paper}
                            onClick={routeChange}
                          >
                            {survey.surveyName}
                          </Paper>{' '}
                        </Link>
                        <Button
                          variant='contained'
                          // className={classes.paper}
                          style={{ width: '100%' }}
                          startIcon={<CheckIcon />}
                          className={classes.closeButton}
                          onClick={() => handleChangeStatue(survey._id)}
                        >
                          Open
                        </Button>
                      </Grid>
                    )
                })}
              </Grid>
            </div>
          </Container>
        </React.Fragment>}
    </React.Fragment>

        
     
   : (
    <div> <Errorhandler message={"Access Denied"} detail={' Please Login First !!'}/></div>
  )
}
const mapStateToProps = state => ({
  userData: state.user.userData,
  error: state.userSurveys.error,
  userSurveys: state.userSurveys.userSurveys,
  allUserSurveys: state.userSurveys.userSurveys
})

export default connect(mapStateToProps, { fetchDashboard,validationErrorExplicitNullDashbd })(Dashboard)
